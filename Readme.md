Organ Deformation estimation using Convolutional Neural Networks
================================================================

This is the code associated with our paper ["Learning Soft Tissue Behavior of Organs for Surgical Navigation with Convolutional Neural Networks"](https://link.springer.com/article/10.1007%2Fs11548-019-01965-7) (published in IJCARS, special IPCAI issue 2019).
When using this code for your own projects, please cite this article.

Project description:
--------------------

We use simulations of random organ-like shapes to train a neural network to estimate soft-tissue deformations. To be able to generalize to new, unseen patient organs, we first discretize (voxelize) the organs and the target displacements at regular (spatial) intervals, giving us 3D grids on which the network trains. The network's input contains a representation of the mesh and the surface displacement of part of the organ surface. The output contains a displacement field for all organ-internal voxels.
For more details, please see the original paper:
[Paper (preprint)](https://arxiv.org/abs/1904.00722)

![Displacement estimation](LiverDeformationSample.png "Liver Deformation Example")
Testing network on a liver mesh. From left to right: 1) Network input contains a representation of the liver shape and partial displacement information of the surface. 2) The displacement of the liver, stemming from a FEM simulation. 3) Estimation of the deformation which our network outputs. Encoded in blue are the magnitudes of the displacements. 4) Magnitude of error in each voxel.

Repository overview:
--------------------

This repository contains code to

1. Voxelize (preprocess) surface meshes to produce input and targets for the CNN (*Tools* folder)
    - simulationGen Tool: Sets up a random boundary conditions
    - vtk2elmer: Write the simulation to files required by Elmer
    - voxelize tool: Discretize simulation result into Grid
2. Generate random meshes and deformations to be used for training (*BatchScripts* folder, depends on 1.)
3. Train and test the CNN (*DeformationNet* folder, depends on 1. and 2.)

Prerequisites:
--------------------

1. Tools folder (Required for generating random data and for voxelizing)
    - VTK (tested with version 8.1)
2. BatchScripts folder (Only required for generating random training data)
    - To generate random meshes, we use Blender3D (tested with Blender version 2.76).
    - To simulate random deformations, we use Gmsh (tested with version 2.10.1) and Elmer (ElmerSolver version 8.3)
    - This process also depends on the Tools folder being built correctly.
3. DefromationNet folder (Training and inference)
    - Requires pytorch (tested with version 1.0.0)

Building:
-------------------------

Only the *Tools* folder requires building.
```bash
mkdir Tools/Build
cd Tools/Build
cmake ../Source/
make
```
If VTK 8.1 is not found, you can pass the directory where you built it by adding -DVTK_DIR=/path/to/VTK8.1/ to the cmake call.

Afterwards, make sure the resulting build folders are in your path (will be required by the bash scripts) or install them to your system by calling `make install`.
```
export PATH=$PATH:$(pwd)/simulationGen
export PATH=$PATH:$(pwd)/vtk2elmer
export PATH=$PATH:$(pwd)/voxelize
```

Generating training data:
-------------------------

Note: These steps are only required if you want to generate random training data. 

1. Ensure the prerequisites for the Tools folder and the BatchScripts folder are installed (listed under 'Prerequisites')
2. Build the Tools folder as described above
3. Change to the BatchScripts folder and execute "batchSimulations.sh". You can pass the number of voxels (should usually be 64) per side of the cube, as well as the number of simulations you want to run.
4. Afterwards, run the "preprocessData.py" script, giving it as input the folder in which the random simulations were generated.

Network Training:
---------------------------------

Change to the DeformationNet folder, and run

```bash
python3 train.py /path/to/trainingdata /path/to/output/folder
```
For example, to train on the included minimal data set (note that this only serves for testing purposes, for a real training scenario you should generate the around 10k data samples):
```bash
python3 train.py ../SampleData TrainedModel
```

You can adjust the number of samples to be used for training and validation by changing the "numTrain" and "numTest" variables in train.py.

Note: If you adjusted the number of voxels per side (default 64) during the creation of the training data above, make sure you also adjust "gridSize" in const.py accordingly.

Network Inference:
---------------------------------

A trained network can be applied to real patient organs using the apply.py script.

```
python3 apply.py /path/to/data /path/to/trained_model /path/to/output/folder
```

To continue from the example above, you could do:
```
python3 apply.py ../SampleData TrainedModel Outputs
```

Changing the variables "startIndex" and "num" in the apply.py script allows you to control which simulations 

Note: Since the network expects the same input as during training, the programs from the Tools folder can be used to preprocess patient meshes.

To view results, you can use Paraview. Load a result voxel grid created by the apply script, then add a "Threshold" filter, and set it up to only show voxels where the signed distance function is smaller than zero (to hide all voxels outside the actual organ).

Contact:
---------------------------------

In case of issues or questions, please contact micha.pfeiffer[at]nct-dresden.de.

TODO:
---------------------------------

- Upload training data?
- Upload trained networks?
- Port voxelize and simulation code to Python (and remove C++ dependencies)?
