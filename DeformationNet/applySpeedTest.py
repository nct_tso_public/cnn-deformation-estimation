import os,sys 
if len(sys.argv) < 2:
    raise ValueError("Usage: python3 applySpeedTest.py PathToData") # PathToModel [StartIndex Num]")

import numpy
import math
import data
from data import SimulatedDataSet
import checkpoint
from const import *
from Network import Network
import torch
import torch.nn as nn
from torch.autograd import Variable
import time
import magnitudeLoss
import generateErrorPlots
import util

dataPath = sys.argv[1]
modelPath = None
#modelPath = sys.argv[2]
#outPath = os.path.dirname("./output")
#if len(sys.argv) > 3:
#    outPath = sys.argv[3]
#if not os.path.exists(outPath):
#    os.makedirs(outPath)

startIndex = 0
num = 2000
#if len(sys.argv) > 3:
    #startIndex = int(sys.argv[3])
#if len(sys.argv) > 4:
    #num = int(sys.argv[4])

#print( "Saving results in: ", outPath )

#autoFindZeroDisplacement = False

net = Network()
#testData = SimulatedDataSet( dataPath, num, startIndex, maxDefThreshold=0.1, augment=False )
testData = SimulatedDataSet( dataPath, num, startIndex, maxDefThreshold=0.1, augment=False) #visPercentage=100 )
testDataLoader = torch.utils.data.DataLoader( testData, batch_size=1, shuffle=False, num_workers=0 )
print("Loaded {:d} test samples".format(len(testData)))
preloadedData = []
for d in testDataLoader:
    preloadedData.append( d )
print("Preloaded data.")

#testDataLoader = torch.utils.data.DataLoader( testData, batch_size=10, shuffle=False, num_workers=0 )
#testData = data.loadSimData( dataPath, num, startIndex )
#testData = data.removeLargeDeformations( testData, 0.1)
#testData = data.toTorch( testData, cuda=True )

#criterion0 = nn.MSELoss(size_average=False)
#criterion1 = nn.MSELoss(size_average=False)
#criterion2 = nn.MSELoss(size_average=False)
#criterion3 = nn.MSELoss(size_average=False)
#criterion0.cuda()
#criterion1.cuda()
#criterion2.cuda()
#criterion3.cuda()

avgPool64to32 = torch.nn.AvgPool3d(2, stride=2)
avgPool64to16 = torch.nn.AvgPool3d(4, stride=4)
avgPool64to8 = torch.nn.AvgPool3d(8, stride=8)

net = net.cuda()
net = torch.nn.DataParallel(net)

#if modelPath is not None:
#    print("Loading from checkpoint in: " + modelPath)
#    startEpoch, trainErrs, testErrs = checkpoint.load( modelPath, net )
#else:
#    raise ValueError( "No path to model given." )


times = []
with torch.no_grad():
    for i in range( 0, len(preloadedData) ):

        d = preloadedData[i]

        ## Forward pass and loss calculation:
        #torch.cuda.synchronize()

        #mesh = d["mesh"].cuda().unsqueeze(0)
        mesh = d["mesh"].cuda()

        torch.cuda.synchronize()
        t2 = time.perf_counter()
        #visDispl = Variable( d["visDispl" + visiblePercentage].cuda() )
        #visDispl = d["visDispl"].cuda().unsqueeze(0)
        visDispl = d["visDispl"].cuda()
        #torch.cuda.synchronize()

        out64, out32, out16, out8 = net.forward( mesh, visDispl, applyOnly=True )

        #torch.cuda.synchronize()
        
        # Download something:
        #out64CPU = out64.cpu()

        #t3 = time.perf_counter()
        X = out64[0,2,32,32,32].cpu()
        #print(X)
        torch.cuda.synchronize()
        t5 = time.perf_counter()

        print(d["path"])
        print( "t5 - t2", t5-t2 )
        print(X)
        
        #print( "\tLoad: {:.2e}s".format( t1-t0 ) )
        times.append( t5 - t2 )

        del mesh, visDispl, out64, out32, out16, out8
        #cudaTensorsToDelete = []
        #for k,v in d.items():
        #    print(k, type(v))
        
        #del d
        
        #if autoFindZeroDisplacement:
        #    if lowestVisDisplLossResult is not None:
        #        del lowestVisDisplLossResult["suffix"]
        #        resultData.append( lowestVisDisplLossResult )


avgTime = sum(times)/len(times)
medianTime = numpy.median(times)

print("Average time per frame: {:.2e}s".format(avgTime) )
print("Average FPS: {:.2e}".format(1/avgTime) )

print("Median time: {:.2e}s".format(medianTime))

