import matplotlib as mpl
mpl.use('Agg')
mpl.use('PS')   # generate postscript output by default

import torch
import traceback
import data
import checkpoint
from data import SimulatedDataSet
from util import *
from torch.autograd import Variable
import torch.optim
from Network import Network
from const import *
import torch.nn as nn
import matplotlib.pyplot as plt
import sys
import os
import time
import torch.cuda
import numpy
import random
import time
import glob
import shutil
import magnitudeLoss

if len(sys.argv) < 3:
    raise ValueError("Wrong number of arguments. Usage:\npython3 train.py PathToData OutputPath [Resume]")

dataPath = sys.argv[1]

outPath = sys.argv[2]
print( "Saving results in " + outPath )
if not os.path.exists(outPath):
    os.makedirs(outPath)

#Copy working files to destination, to have a copy of them ready for applying:
for f in glob.glob("./*.py"):
    print("Making backup copy of:", f)
    shutil.copy( f, outPath )

maximumAllowedDeformation = 0.1     # Ignore samples with deformation larger than 10cm
si = 0
numTrain = 9000
numTest = 1000
trainData = SimulatedDataSet( dataPath, numTrain, startIndex=si, maxDefThreshold=maximumAllowedDeformation, augment=True )
testData = SimulatedDataSet( dataPath, numTest, startIndex=numTrain+si, maxDefThreshold=maximumAllowedDeformation, augment=False )

batch_size = 10
trainDataLoader = torch.utils.data.DataLoader( trainData,
        batch_size=batch_size,
        #sampler=torch.utils.data.sampler.WeightedRandomSampler( trainData.drawingWeights, len(trainData) ),
        shuffle=True,
        num_workers=8 )

testDataLoader = torch.utils.data.DataLoader( testData,
        batch_size=batch_size,
        #sampler=torch.utils.data.sampler.WeightedRandomSampler( testData.drawingWeights, len(testData) ),
        shuffle=False,
        num_workers=8 )

#visiblePercentage = "75"

#trainData = data.removeLargeDeformations( trainData, 0.1)
#testData = data.removeLargeDeformations( testData, 0.1)
#trainData = data.augment( trainData )
#testData = data.augment( testData )
#trainData = data.toTorch( trainData, cuda=True )
#testData = data.toTorch( testData, cuda=True )
#learningRate = 1e-4
#learningRate = 1e-7
#learningRate = 1e-5     # Seems to be good for Augmented, 10000 samples
learningRate = 2e-5
#learningRate = 5e-5
#learningRate = 5e-6     # Seems to be good for SmallerForces, 10000 samples
print( "Number of training samples: {:d}".format(len(trainData)) )
print( "Number of test samples: {:d}".format(len(testData)) )
resumePath = None
if len(sys.argv) > 3:
    resumePath = sys.argv[3]

trainErrs = [[],[],[],[],[],[]]
testErrs = [[],[],[],[],[],[]]

#plt.clf()
#plt.plot( trainData.drawingWeights, '-', label = "drawingWeights", color = (0.8,0,0.8) )
#plt.savefig( outPath + "/trainData" )

def plotErrors( trainErr, testErr ):
    for i in range(0,6):
        trainErrs[i].append( trainErr[i] )
        testErrs[i].append( testErr[i] )

    plt.clf()
    
    plt.plot( trainErrs[5], '-', label = "train total", color = (0.8,0,0.8) )
    plt.plot( trainErrs[4], '-', label = "train visible", color = (0.4,0,0.8) )
    for i in range(0,4):
        resolution = 2**(6-i)
        plt.plot( trainErrs[i], label = "train {}".format(resolution),
                color = (0,0, 1-i/6), linestyle = "dashed" )

    plt.plot( testErrs[5], '-', label = "test total", color = (0.8,0.8,0) )
    plt.plot( testErrs[4], '-', label = "test visible", color = (0.8,0,0.4) )
    for i in range(0,4):
        resolution = 2**(6-i)
        plt.plot( testErrs[i], label = "test {}".format(resolution),
                color = (0, 1-i/6,0), linestyle = "dashed" )

    plt.yscale('log')
    plt.grid(True)
    plt.legend()
    plt.savefig( outPath + "/errors" )

net = Network()
net.cuda()
net = torch.nn.DataParallel(net)

#optimizer = torch.optim.Adam( net.parameters(), lr = learningRate, weight_decay=5e-9 )
#optimizer = torch.optim.Adam( net.parameters(), lr = learningRate, weight_decay=1e-7 )
optimizer = torch.optim.Adam( net.parameters(), lr = learningRate )
#scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma = 0.995 )
#scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma = 0.985 )
scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma = 1 )

startEpoch = 0
# If we are to resume, load network parameters, optimizer and scheduler:
if resumePath is not None:
    print("Resuming from checkpoint in: " + resumePath)
    try:
        startEpoch, trainErrs, testErrs = checkpoint.load( resumePath, net, optimizer, scheduler )
    except Exception as ex:
        print("Could not resume from checkpoint.", ex)
        optimizer = torch.optim.Adam( net.parameters(), lr = learningRate )
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma = 1 )
        net = Network()
        net.cuda()
        net = torch.nn.DataParallel(net)
        trainErrs = [[],[],[],[],[],[]]
        testErrs = [[],[],[],[],[],[]]
        startEpoch = 0

avgPool64to32 = torch.nn.AvgPool3d(2, stride=2)
avgPool64to16 = torch.nn.AvgPool3d(4, stride=4)
avgPool64to8 = torch.nn.AvgPool3d(8, stride=8)

#criterion0 = nn.MSELoss(size_average=False)
#criterion1 = nn.MSELoss(size_average=False)
#criterion2 = nn.MSELoss(size_average=False)
#criterion3 = nn.MSELoss(size_average=False)
#criterion0.cuda()
#criterion1.cuda()
#criterion2.cuda()
#criterion3.cuda()


#for state in optimizer.state.values():
    #for k, v in state.items():
        #if isinstance(v, torch.Tensor):
            #state[k] = v.cuda()

def test():
    net.eval()

    lossSum = numpy.array([0.0,0.0,0.0,0.0,0.0,0.0])

    #for i in range( 0, len(testData) ):
    for i_batch, d in enumerate(testDataLoader):
        try:

            # Forward pass and loss calculation:
            mesh = Variable( d["mesh"].cuda()  )
            #visDispl = Variable( d["visDispl" + visiblePercentage].cuda() )
            visDispl = Variable( d["visDispl"].cuda() )
            target64 = Variable( d["target"].cuda() , requires_grad=False )
            target32 = avgPool64to32( target64 )
            target16 = avgPool64to16( target64 )
            target8 = avgPool64to8( target64 )

            out64, out32, out16, out8 = net.forward( mesh, visDispl )

            visMask = visDispl.abs().sum( dim=1 ) > 0

            loss, l64, l32, l16, l8, lVis = magnitudeLoss.loss( out64, out32, out16, out8,
                    target64, target32, target16, target8, visMask.float() )

            # Individual errors:
            lossSum[0] += l64.item()
            lossSum[1] += l32.item()
            lossSum[2] += l16.item()
            lossSum[3] += l8.item()
            lossSum[4] += lVis.item()
            # Total err:
            lossSum[5] += loss.item()

            del mesh, visDispl, target64, target32, target16, loss, l64, l32, l16, l8, lVis, out64, out32, out16, out8
        except Exception as err:
            print(traceback.format_exc(), err)
            print( "Error: Failed to process batch:", i_batch )

    avgTestErrs = lossSum/len(testData)

    return avgTestErrs

for epoch in range( startEpoch, 9999 ):

    lossSum = numpy.array([0.0,0.0,0.0,0.0,0.0,0.0])

    shuffleIndex = torch.randperm( len(trainData) )

    net.train()

    #secondaryLossFactor = max( (10-epoch)/10, 0 )
    #secondaryLossFactor = 1/(epoch+1)

    t1 = time.time()
    #for i in range( 0, len(trainData) ):
    for i_batch, d in enumerate(trainDataLoader):
        printProgressBar( i_batch+1, len(trainDataLoader), "Epoch {}:".format( epoch ), decimals=2 )
        try:
            optimizer.zero_grad()

            # Forward pass and loss calculation:
            mesh = Variable( d["mesh"].cuda() )
            #visDispl = Variable( d["visDispl" + visiblePercentage].cuda() )
            visDispl = Variable( d["visDispl"].cuda() )
            target64 = Variable( d["target"].cuda(), requires_grad=False )
            target32 = avgPool64to32( target64 )
            target16 = avgPool64to16( target64 )
            target8 = avgPool64to8( target64 )

            out64, out32, out16, out8 = net.forward( mesh, visDispl )

            visMask = visDispl.abs().sum( dim=1 ) > 0

            loss, l64, l32, l16, l8, lVis = magnitudeLoss.loss( out64, out32, out16, out8,
                    target64, target32, target16, target8, visMask.float() )

            # Individual errors:
            lossSum[0] += l64.item()
            lossSum[1] += l32.item()
            lossSum[2] += l16.item()
            lossSum[3] += l8.item()
            lossSum[4] += lVis.item()
            # Total err:
            lossSum[5] += loss.item()

            loss.backward()
            optimizer.step()

            del mesh, visDispl, target64, target32, target16, loss, l64, l32, l16, l8, lVis, out64, out32, out16, out8
        except Exception as err:
            print(traceback.format_exc(), err)
            print( "Error: Failed to process batch:", i_batch )

    avgTrainErrs = lossSum/len(trainData)

    avgTestErrs = test()

    dt = time.time() - t1

    print( epoch, " Loss: {:e} {:e} {:e} {:e} Vis: {:e} \tTotal: {:e}".format(
                    avgTrainErrs[0], avgTrainErrs[1], avgTrainErrs[2], avgTrainErrs[3], avgTrainErrs[4],
                    avgTrainErrs[5] ),
                    "\n\tTest: {:e} {:e} {:e} {:e} Vis: {:e} \tTotal: {:e}".format(
                    avgTestErrs[0], avgTestErrs[1], avgTestErrs[2], avgTestErrs[3], avgTestErrs[4],
                    avgTestErrs[5] ),
                    "\n\tLearning Rate: {:.2e}, Time: {:2e}".format( optimizer.param_groups[0]['lr'], dt ) )

    plotErrors( avgTrainErrs, avgTestErrs )

    #plt.clf()
    #plt.hist( trainData.drawn, int(len(trainData)/8) )
    #plt.grid(True)
    #plt.savefig( outPath + "/drawing" )

    scheduler.step()    # Decrease Learning Rate

    #if epoch % 5 == 0:
    #torch.save(net.state_dict(), outPath + "/model" )
    checkpoint.save( outPath, epoch, net, optimizer, scheduler, trainErrs, testErrs )


