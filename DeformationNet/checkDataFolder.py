import os
import sys
import shutil

dataPath = sys.argv[1]

i = 0
while True:

    folder = dataPath + "/{:05d}/".format(i)
    filenameInput = dataPath + "/{:05d}/voxels_input.vtu".format(i)
    filenameTarget = dataPath + "/{:05d}/voxels_result.vtu".format(i)

    if not os.path.exists( folder ):
        break
    
    if not os.path.isfile(filenameTarget) or not os.path.isfile(filenameTarget):
        filenameInputPrev = dataPath + "/{:05d}/voxels_input.vtu".format(i-1)
        filenameTargetPrev = dataPath + "/{:05d}/voxels_result.vtu".format(i-1)
        shutil.copyfile( filenameInputPrev, filenameInput )
        shutil.copyfile( filenameTargetPrev, filenameTarget )
        print("Found broken simulation " + folder + ". Replaced with " + str(i-1) + ".")
        #raise ValueError("Missing file: " + str(i))
    #elif not os.path.isfile(filenameTarget):
        #raise ValueError("Missing file: " + str(i))
    i += 1
