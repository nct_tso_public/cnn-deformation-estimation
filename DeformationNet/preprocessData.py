import sys
if len(sys.argv) < 3:
    raise ValueError("Usage: PathToData Num [StartNum]")

dataPath = sys.argv[1]
num = int(sys.argv[2])
startNum = 0
if len(sys.argv) > 3:
    startNum = int(sys.argv[3])

import data

valid = 0
for i in range( startNum, num+startNum ):
    print(str(i) + "/" + str(num))
    if data.preprocess( dataPath, i ):
        valid += 1

print( "Converted {:d} samples.".format( valid ) )
