import os,sys 

import numpy
import vtk
from vtk.util import numpy_support

import matplotlib as mpl
mpl.use('Agg')
mpl.use('PS')   # generate postscript output by default

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.colors import LogNorm
#import matplotlib as mpl
#mpl.rc('image', cmap='Greys')
from matplotlib.ticker import LogLocator
from mpl_toolkits.axes_grid1 import AxesGrid
import matplotlib.ticker as ticker
from matplotlib import gridspec


def binNumber( val, minimum, maximum, numberOfBins ):
  binWidth = (maximum-minimum)/numberOfBins
  print("BinWidth:", binWidth)
  b = int((val-minimum)/(maximum-minimum)*numberOfBins)
  if b == numberOfBins:
    b = numberOfBins - 1
  print("val", val, "bin:", b)
  return b

def parseDirectorySortByVisDispl( path ):

    print("Sorting errors by visible surface amount")
    
    outPath = path + "/metrics/"
    if not os.path.exists( outPath ):
        os.makedirs( outPath )

    errors = []
    visPercentages = []

    for i in range(1, 2000):
        p = path + "/{:05d}/".format(i)
        print(p)
        if not os.path.exists( p ):
            continue

        filename = p + "voxels_output.vtu"

        reader = vtk.vtkXMLUnstructuredGridReader()
        reader.SetFileName(filename)
        reader.Update()
        voxelsOutput = reader.GetOutput()

        if not voxelsOutput.GetFieldData().HasArray("visibleAreaSize"):
            print("no visibleAreaSize")
            continue
        
        visibleAreaSize = int(voxelsOutput.GetFieldData().GetArray("visibleAreaSize").GetTuple1(0))
        print("Visible Area Size:", visibleAreaSize)


        cellData = voxelsOutput.GetCellData()
        displacementError = numpy_support.vtk_to_numpy( cellData.GetArray("displacementError") )
        sdf = numpy_support.vtk_to_numpy( cellData.GetArray("signedDistance") )
        err = displacementError[sdf<0].mean()
        print("displacementError", err, displacementError.mean())
        visibleVoxels = numpy_support.vtk_to_numpy( cellData.GetArray("visibleVoxels") )

        arrName = "isVisible{:d}".format( visibleAreaSize )
        isVisible = numpy_support.vtk_to_numpy( cellData.GetArray(arrName) )

        print(visibleVoxels.shape)
        print(isVisible.shape)
        visPercentage = 100*isVisible.sum()/visibleVoxels.sum()
        print( "visible percentage:", visPercentage )

        print(isVisible.max(), isVisible.min(), isVisible.sum())

        visPercentages.append( visPercentage )
        errors.append( err )

   
    numOfBins = 10
    binWidth = 100/numOfBins
    visPercentageBins = []
    errorsBinned = []
    for i in range(0, numOfBins):
        errorsBinned.append( [] )

    for i in range( 0, len(visPercentages) ):
        err = errors[i]
        curBin = binNumber( visPercentages[i], 0, 100, numOfBins )
        print( curBin, i, visPercentages[i] )
        errorsBinned[curBin].append(err * 1e2)

    errorsAveraged = []
    errorsMedian = []
    binSizes = []
    for i in range(0, numOfBins):
        if len(errorsBinned[i]) > 0:
            errorsAveraged.append( sum(errorsBinned[i])/len(errorsBinned[i]) )
            errorsMedian.append( numpy.median(errorsBinned[i]) )
            binSizes.append( len( errorsBinned[i] ) )
            visPercentageBins.append( i*binWidth+5 )
        #else:
        #    errorsAveraged.append(0)

    fig = plt.figure("fig", figsize=(4, 1.5))
    plt.clf()
    plt.plot( visPercentageBins, errorsAveraged, '-o' )#, color = (1,0.2,0.2) )
        #plt.plot( myY, median, '--', label = "median {:.0f}cm".format(dist*100), color = col )
    plt.legend()
    plt.xlim( [0,100] )
    plt.ylim( [0,0.6] )
    plt.xticks( [0.0,0.2,0.4,0.6] )
    plt.xticks( numpy.arange( 0, 100, step=10 ) )
    plt.xlabel( "Visible surface (%)" )
    plt.ylabel( "Avg. error [cm]" )
    plt.grid(True)
    plt.savefig( outPath + "/errSortedByVisDisplAvg", bbox_inches = "tight")

    plt.clf()
    plt.plot( visPercentageBins, errorsMedian, '-o' )#, color = (1,0.2,0.2) )
        #plt.plot( myY, median, '--', label = "median {:.0f}cm".format(dist*100), color = col )
    plt.legend()
    plt.xlim( [0,100] )
    #plt.ylim( [0,0.4] )
    plt.xlabel( "Visible surface (%)" )
    plt.ylabel( "Median error (cm)" )
    plt.grid(True)
    plt.savefig( outPath + "/errSortedByVisDisplMedian", bbox_inches = "tight" )
        
    plt.clf()
    plt.plot( visPercentageBins, binSizes, '-o' )#, color = (1,0.2,0.2) )
        #plt.plot( myY, median, '--', label = "median {:.0f}cm".format(dist*100), color = col )
    plt.legend()
    plt.xlim( [0,100] )
    plt.xlabel( "Visible surface (%)" )
    plt.ylabel( "Samples in bin" )
    plt.grid(True)
    plt.savefig( outPath + "/errSortedByVisDisplBinSizes", bbox_inches = "tight" )
       
    pos = numpy.arange( 5, (numOfBins)*binWidth+5, binWidth )
    print( "bins:", len(errorsBinned), len(visPercentageBins), len(pos) )
    plt.clf()
    plt.boxplot( errorsBinned, positions = pos, widths=2 )
    plt.xlabel( "Visible surface (%)" )
    plt.ylabel( "Avg. errors [cm]" )
    plt.xlim( [0,100] )
    plt.grid(True)
    plt.xticks( numpy.arange( 0, 100, step=10 ) )
    plt.savefig( outPath + "/errSortedByVisDisplBox", bbox_inches = "tight" )

    errorsSorted = [x for _, x in sorted(zip(visPercentages,errors), key=lambda pair: pair[0])]
    visPercentagesSorted = sorted(visPercentages)
    #print(errorsSorted)
    #print(visPercentagesSorted)

    plt.clf()
    plt.plot( visPercentagesSorted, errorsSorted, '-', color = (1,0.2,0.2) )
        #plt.plot( myY, median, '--', label = "median {:.0f}cm".format(dist*100), color = col )
    plt.legend()
    plt.xlabel( "Visible surface (%)" )
    plt.ylabel( "Mean mag. of error" )
    plt.grid(True)
    plt.savefig( outPath + "/errSortedByVisDispl" )



if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise ValueError("Usage: python3 generateErrorPlots.py PathToResultFolder")
  
    outPath = sys.argv[1]
    print( "Loading from: " + outPath )
    parseDirectorySortByVisDispl( outPath )





