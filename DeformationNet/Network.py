import torch
from torch.autograd import Variable
import torch.nn as nn
from const import *
import math
import time

class Network(nn.Module):
    
    def __init__(self):
        super(Network, self).__init__()

        self.conv64_0 = nn.Conv3d( 5, 16, 3, padding=1 )
        self.conv64_1 = nn.Conv3d( 16, 16, 3, padding=1 )
        #self.conv0_2 = nn.Conv3d( 128, 64, 3, padding=1 )

        self.conv32_0 = nn.Conv3d( 16, 64, 3, padding=1 )
        self.conv32_1 = nn.Conv3d( 64, 64, 3, padding=1 )
        #self.conv1_2 = nn.Conv3d( 128, 128, 3, padding=1 )

        self.conv16_0 = nn.Conv3d( 64, 128, 3, padding=1 )
        self.conv16_1 = nn.Conv3d( 128, 128, 3, padding=1 )
        #self.conv2_2 = nn.Conv3d( 256, 256, 3, padding=1 )

        self.conv8_0 = nn.Conv3d( 128, 256, 3, padding=1 )
        self.conv8_1 = nn.Conv3d( 256, 256, 3, padding=1 )
        self.conv8_2 = nn.Conv3d( 256, 256, 3, padding=1 )
        self.conv8_3 = nn.Conv3d( 256, 256, 3, padding=1 )
        self.conv8_4 = nn.Conv3d( 256, 128, 3, padding=1 )

        #self.conv1_0 = nn.Conv3d( 256, 512, 5, padding=2 )
        #self.conv3_1 = nn.Conv3d( 512, 512, 5, padding=2 )
        #self.conv3_2 = nn.Conv3d( 512, 512, 3, padding=1 )

        #self.upConv3to2 = nn.ConvTranspose3d( 512, 256, 2, stride=2 )
        #self.combineConv2_0 = nn.Conv3d( 512, 256, 3, padding=1 )
        #self.combineConv2_1 = nn.Conv3d( 256, 256, 3, padding=1 )
        #self.upConv8to16 = nn.ConvTranspose3d( 512, 256, 4, stride=2, padding=1 )
        self.upConv8to16 = nn.Upsample( scale_factor=2 )
        self.combineConv16_0 = nn.Conv3d( 128+128, 128, 3, padding=1 )
        self.combineConv16_1 = nn.Conv3d( 128, 64, 3, padding=1 )

        #self.upConv16to32 = nn.ConvTranspose3d( 256, 64, 4, stride=2, padding=1 )
        self.upConv16to32 = nn.Upsample( scale_factor=2 )
        self.combineConv32_0 = nn.Conv3d( 64+64, 32, 3, padding=1 )
        self.combineConv32_1 = nn.Conv3d( 32, 16, 3, padding=1 )

        #self.upConv32to64 = nn.ConvTranspose3d( 64, 16, 4, stride=2, padding=1 )
        self.upConv32to64 = nn.Upsample( scale_factor=2 )
        self.combineConv64_0 = nn.Conv3d( 16+16+5, 16, 3, padding=1 )
        self.combineConv64_1 = nn.Conv3d( 16, 16, 3, padding=1 )
        self.combineConv64_2 = nn.Conv3d( 16, 3, 1 )

        self.conv8toOutput = nn.Conv3d( 128, 3, 1)#, padding=1 )
        self.conv16toOutput = nn.Conv3d( 64, 3, 1)#, padding=1 )
        self.conv32toOutput = nn.Conv3d( 16, 3, 1)#, padding=1 )

        #self.avgPool32to16 = torch.nn.AvgPool3d(2, stride=2)
        #self.avgPool32to16 = torch.nn.AvgPool3d(2, stride=2)
        #self.avgPool2to3 = torch.nn.AvgPool3d(2, stride=2)

        self.avgPool64to32 = torch.nn.AvgPool3d(2, stride=2)
        self.avgPool64to16 = torch.nn.AvgPool3d(4, stride=4)
        self.avgPool64to8 = torch.nn.AvgPool3d(8, stride=8)

        self.avgPool32to16 = torch.nn.AvgPool3d(2, stride=2)
        self.avgPool16to8 = torch.nn.AvgPool3d(2, stride=2)

        #self.dropout = nn.Dropout( 0.25 )

        self.nonLin = nn.Softsign()

    def time( self, label=None ):
        if self.timing:
            torch.cuda.synchronize()
            if label is not None:
                print( label, time.perf_counter() - self.dt )
            self.dt = time.perf_counter()

    def forward( self, mesh, visDispl, applyOnly=False ):

        self.timing = False
        self.time()

        config = torch.cat( ( mesh*0.1, visDispl ), 1 )

        self.time( "t1" )
        #config = config.unsqueeze( 0 )

        res64 = self.nonLin( self.conv64_0( config ) )
        #res64 = self.nonLin( self.conv64_1( res64 ) )
        #res0 = self.nonLin( self.conv0_2( res0 ) )
        self.time( "t2" )

        #res0 = self.dropout( res0 )

        res32 = self.avgPool64to32( res64 )
        res32 = self.nonLin( self.conv32_0( res32 ) )
        res32 = self.nonLin( self.conv32_1( res32 ) )
        #res32 = self.nonLin( self.conv1_2( res1 ) )

        self.time( "t3" )
        res16 = self.avgPool32to16( res32 )
        res16 = self.nonLin( self.conv16_0( res16 ) )
        res16 = self.nonLin( self.conv16_1( res16 ) )
        #res2 = self.nonLin( self.conv2_2( res2 ) )
        self.time( "t4" )

        res8 = self.avgPool16to8( res16 )
        res8 = self.nonLin( self.conv8_0( res8 ) )
        res8 = self.nonLin( self.conv8_1( res8 ) )
        res8 = self.nonLin( self.conv8_2( res8 ) )
        res8 = self.nonLin( self.conv8_3( res8 ) )
        res8 = self.nonLin( self.conv8_4( res8 ) )
        self.time( "t5" )


        res16Up = self.upConv8to16( res8 )
        res16 = torch.cat( (res16, res16Up), 1 )
        res16 = self.nonLin( self.combineConv16_0( res16 ) )
        res16 = self.nonLin( self.combineConv16_1( res16 ) )
        self.time( "t6" )

        res32Up = self.upConv16to32( res16 )
        self.time( "t7_0" )
        res32 = torch.cat( (res32, res32Up), 1 )
        self.time( "t7_1" )
        res32 = self.nonLin( self.combineConv32_0( res32 ) )
        self.time( "t7_2" )
        res32 = self.nonLin( self.combineConv32_1( res32 ) )
        self.time( "t7" )
       
        res64Up = self.upConv32to64( res32 )
        self.time( "t8_0" )
        res64 = torch.cat( (res64, res64Up, config), 1 )
        res64 = self.nonLin( self.combineConv64_0( res64 ) )
        res64 = self.nonLin( self.combineConv64_1( res64 ) )
        res64out = self.nonLin( self.combineConv64_2( res64 ) )
        self.time( "t8" )

        # Lower resolution outputs for additional error terms:
        res32out = self.nonLin( self.conv32toOutput( res32 ) )
        res16out = self.nonLin( self.conv16toOutput( res16 ) )
        res8out = self.nonLin( self.conv8toOutput( res8 ) )
        self.time( "t9" )
        #res1out = self.conv1toOutput( res1 )
        #res2out = self.conv2toOutput( res2 )

        #res0out = res0out + visDispl
        #es1out = res1out + self.avgPool0to1( visDispl )
        #es2out = res2out + self.avgPool0to2( visDispl )
        #es3out = res3out + self.avgPool0to3( visDispl )

        # Generate mask from signed distance function:
        sdf = mesh.narrow(1,0,1)
        mask = sdf.lt( 0 )
        mask = mask.expand( -1,3,-1,-1,-1 ).float()
        self.time( "t10" )

        res64out = res64out*mask
        res32out = res32out*self.avgPool64to32( mask )
        res16out = res16out*self.avgPool64to16( mask )
        res8out = res8out*self.avgPool64to8( mask )
        self.time( "t11" )

        #return res0out*1e2, res1out*1e2, res2out*1e2
        return res64out, res32out, res16out, res8out

if __name__ == "__main__":
    net = Network()

    num = 0
    for name, p in net.named_parameters():
        print( name, p.numel(), p.requires_grad )
        num += p.numel()
    print("Network parameters:", num)
