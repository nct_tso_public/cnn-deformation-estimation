import torch

mse = torch.nn.MSELoss()
mseVis = torch.nn.MSELoss( reduce=False )

def loss( o64, o32, o16, o8, t64, t32, t16, t8, visMask ):

    l64 = mse( o64, t64 )
    l32 = mse( o32, t32 )
    l16 = mse( o16, t16 )
    l8 = mse( o8, t8 )
    #print(l64, l32, l16, l8)

    # Calculate the squared magnitude of the difference:
    #diff64 = o64 - t64
    #diff32 = o32 - t32
    #diff16 = o16 - t16
    #diff8 = o8 - t8

    #magSqu64 = (diff64**2).sum( dim=1 )
    #magSqu32 = (diff32**2).sum( dim=1 )
    #magSqu16 = (diff16**2).sum( dim=1 )
    #magSqu8 = (diff8**2).sum( dim=1 )

    # Vis loss large, divided by number of visible voxels:
    #err64 = mseVis(o64, t64).sum(1)
    #errVis = err64*visMask
    #visMask = visMask.view( visMask.shape[0], -1 )
    #nonzero = visMask.shape[1] - (visMask == 0).sum(1)
    #errVis = errVis.view( errVis.shape[0], -1 ).sum(1)
    #lVis = torch.div( errVis, nonzero.float() ).sum()

    #visMask = visMask.unsqueeze(1)
    #lVis = mse(o64*visMask, t64*visMask)
    lVis = torch.Tensor( [0] )

    #print( "view:", err0.view( err0.size(0), -1 ).size() )
    # Mean over the voxels
    #meanErr64 = magSqu64.view( magSqu64.size(0), -1 ).mean( dim=1 )
    #meanErr32 = magSqu32.view( magSqu32.size(0), -1 ).mean( dim=1 )
    #meanErr16 = magSqu16.view( magSqu16.size(0), -1 ).mean( dim=1 )
    #meanErr8 = magSqu8.view( magSqu8.size(0), -1 ).mean( dim=1 )

    #meanErrVis = magSquVis.view( magSquVis.size(0), -1 ).mean( dim=1 )

    # Mean over the batch:
    #if reduce:
    #l64 = meanErr64.mean()
    #l32 = meanErr32.mean()
    #l16 = meanErr16.mean()
    #l8 = meanErr8.mean()
    #lVis = meanErrVis.mean()
    #else:
    # Sum over the batch:
    #l64 = meanErr64.sum()
    #l32 = meanErr32.sum()
    #l16 = meanErr16.sum()
    #l8 = meanErr8.sum()
    #lVis = meanErrVis.sum()

    # Weight in all the losses at individual resolutions into one final float:
    l = l64 + l32 + l16 + l8# + 100*lVis
    #l = l64# + 3*l32 + 2*l16 + l8# + 100*lVis
    del visMask   
    return l, l64, l32, l16, l8, lVis


