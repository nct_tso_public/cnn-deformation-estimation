import numpy
import data
import sys,os
import vtk
import math

if len(sys.argv) < 3:
    raise ValueError("Usage: python3 PathToData num [outPath]")

import matplotlib as mpl
mpl.use('Agg')
mpl.use('PS')   # generate postscript output by default
import matplotlib.pyplot as plt


dataPath = sys.argv[1]
num = int(sys.argv[2])
outPath = "Analyzer"
if len(sys.argv) > 3:
    outPath = sys.argv[3]
if not os.path.exists(outPath):
    os.makedirs(outPath)

testData = data.loadSimData( dataPath, num )
l = len(testData)
#testData = data.removeLargeDeformations( testData, 0.1)
#print("Removed samples because of large deformations:", l - len(testData) )
#l = len(testData)
#testData = data.removeZeroDeformations( testData )
#print("Removed samples because of deformation without force:", l - len(testData) )
#l = len(testData)
#testData = data.augment( testData )
#print("Augmentation added {:d} data samples.".format( len(testData) - l ) )
#print("Number of test samples:", len(testData) )

maxDeformations = []
maxDefOverall = -1
maxDefOverallIndex = -1
zeroDeformation = 0
maxManipulation = []
maxManipulationOverall = 0
noManipulationDeformation = 0
for i in range( 0, len(testData) ):
    d = testData[i]
    maxDef = numpy.linalg.norm( d["target"], 2, 0 ).max()
    if maxDef > maxDefOverall:
        maxDefOverall = maxDef
        maxDefOverallIndex = d["path"]
    if maxDef < 1e-7:
        zeroDeformation += 1
    maxDeformations.append( maxDef )
    #maxF = numpy.linalg.norm( d["force"], 2, 0 ).max()
    #if maxF < 1e-10 and maxDef > 0:
        #noManipulationDeformation += 1
        #print("No force, but deformation in sample {:d}!!".format(i))
    #maxManipulation.append(maxF)

    maxFixedDisplacement = d["mesh"][1,:,:,:].max()
    if maxFixedDisplacement < 1e-3:
        print("Error: No fixed displacement in sample {:d}!!".format(i))

    try:
        ugReader = vtk.vtkXMLUnstructuredGridReader()
        ugReader.SetFileName( d["path"] + "/simulation.vtu")
        ugReader.Update()

        ugrid = ugReader.GetOutput()
        manipArray = None
        for i in range(0,ugrid.GetPointData().GetNumberOfArrays()):
            if ugrid.GetPointerData().GetArrayName(i) == "manipulation":
                manipArray = ugrid.GetPointerData().GetArray(i)
        for i in range(0,ugrid.GetCellData().GetNumberOfArrays()):
            if ugrid.GetCellData().GetArrayName(i) == "manipulation":
                manipArray = ugrid.GetCellData().GetArray(i)

        
        for i in range(0,manipArray.GetNumberOfTuples()):
            x,y,z = manipArray.GetTuple3(i)
            if x != 0 or y != 0 or z != 0:
                mag = math.sqrt(x*x + y*y + z*z)
                maxManipulation.append( mag )        
                break

    except Exception as ex:
        print(ex)

if len(maxManipulation) > 0:
    plt.clf()
    plt.plot( maxManipulation, maxDeformations, "." )
    plt.grid(True)
    plt.xlabel("Pressure Magnitude")
    plt.ylabel("Maximum Deformation Magnitude")
    plt.savefig( outPath + "/deformationsOverPressure" )


print( "Maximum Deformation:", maxDefOverall, maxDefOverallIndex )
print( "Number of Samples which have no deformation: {:d} ({:f}%)".format(zeroDeformation,100*zeroDeformation/len(testData)) )
plt.clf()
plt.hist( maxDeformations, bins=50 )
plt.grid(True)
plt.savefig( outPath + "/maxDeformations" )
print("Saved " + outPath + "/maxDeformations" )
#plt.clf()
#plt.hist( maxManipulation, bins=50 )
#plt.grid(True)
#plt.savefig( outPath + "/maxManipulation" )
#print("Saved " + outPath + "/maxManipulation" )

