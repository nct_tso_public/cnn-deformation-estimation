import torch
from torch.autograd import Variable
from const import *
from Network import Network
import torch.nn.functional as F
from torch.autograd import Variable
from torchviz import make_dot

net = Network()

mesh = Variable( torch.randn( 2, gridSize, gridSize, gridSize ), requires_grad = True )
force = Variable( torch.randn( 3, gridSize, gridSize, gridSize ), requires_grad = True )
out0,out1,out2 = net.forward( mesh, force )

outsum = out0.sum() + out1.sum() + out2.sum()

#dot = make_dot( outsum, params=dict(list(net.named_parameters()) + [("mesh", mesh), ("force", force)]))
dot = make_dot( outsum, params=dict(list(net.named_parameters()) + [("mesh", mesh), ("force", force)]))
dot.render("model.gv")
