import data
from util import *

dataPath = sys.argv[1]

trainData = data.toTorch( data.loadSimData( dataPath, 1 ), cuda=True )
augmentedData = []

def flipSample( d, flipDim ):
    df = {}
    df["mesh"] = flip( d["mesh"], flipDim+1 )
    df["force"] = flip( d["force"], flipDim+1 )
    df["target"] = flip( d["target"], flipDim+1 )
    df["force"][flipDim,:,:,:] = - df["force"][flipDim,:,:,:]
    df["target"][flipDim,:,:,:] = - df["target"][flipDim,:,:,:]
    df["path"] = d["path"]
    return df


dfX = flipSample( trainData[0], 0 )
dfY = flipSample( trainData[0], 1 )
dfZ = flipSample( trainData[0], 2 )
dfXX = flipSample( dfX, 0 )
dfYY = flipSample( dfY, 1 )
dfZZ = flipSample( dfZ, 2 )

dfXY = flipSample( dfX, 1 )

augmentedData.append( dfX )
augmentedData.append( dfY )
augmentedData.append( dfZ )
augmentedData.append( dfXX )
augmentedData.append( dfYY )
augmentedData.append( dfZZ )
augmentedData.append( dfXY )

print("Save original:")
data.save( trainData, "orig" )
print("Save flipped:")
data.save( augmentedData, "flipped" )


