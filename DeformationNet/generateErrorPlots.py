import os,sys 

import numpy

import matplotlib as mpl
mpl.use('Agg')
mpl.use('PS')   # generate postscript output by default

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.colors import LogNorm
#import matplotlib as mpl
#mpl.rc('image', cmap='Greys')
from matplotlib.ticker import LogLocator
from mpl_toolkits.axes_grid1 import AxesGrid
import matplotlib.ticker as ticker
from matplotlib import gridspec

def parseDirectory( path ):

    outPath = path + "/metrics/"
    if not os.path.exists( outPath ):
        os.makedirs( outPath )

    binsDist = 30
    binsDeform = 10
    maxDist = 0.3
    maxDeform = 0.1

    x = numpy.linspace(0, maxDist*100, binsDist)
    y = numpy.linspace(0, maxDeform*100, binsDeform)
    X, Y = numpy.meshgrid(x, y)

    minErr = numpy.load( path + "/errorMetricsMin.npy" )
    maxErr = numpy.load( path + "/errorMetricsMax.npy")
    meanErr = numpy.load( path + "/errorMetricsMean.npy")
    medianErr = numpy.load( path + "/errorMetricsMedian.npy")

    minErrRel = numpy.load( path + "/errorMetricsMinRel.npy")
    maxErrRel = numpy.load( path + "/errorMetricsMaxRel.npy")
    meanErrRel = numpy.load( path + "/errorMetricsMeanRel.npy")
    medianErrRel = numpy.load( path + "/errorMetricsMedianRel.npy")

    fig = plt.figure()

    plt.clf()
    ax = fig.gca(projection='3d')
    ax.plot_surface( X, Y, minErr[:,:].squeeze().transpose()*100, cmap=cm.coolwarm )
    ax.set_xlabel("dist from visible (cm)")
    ax.set_ylabel("deformation (cm)")
    ax.set_zlabel("min err (cm)")
    plt.savefig( outPath + "/errMin" )

    plt.clf()
    ax = fig.gca(projection='3d')
    ax.plot_surface( X, Y, maxErr[:,:].squeeze().transpose()*100, cmap=cm.coolwarm )
    ax.set_xlabel("dist from visible (cm)")
    ax.set_ylabel("deformation (cm)")
    ax.set_zlabel("max err (cm)")
    plt.savefig( outPath + "/errMax" )

    plt.clf()
    ax = fig.gca(projection='3d')
    ax.plot_surface( X, Y, meanErr[:,:].squeeze().transpose()*100, cmap=cm.coolwarm )
    ax.set_xlabel("dist from visible (cm)")
    ax.set_ylabel("deformation (cm)")
    ax.set_zlabel("mean err (cm)")
    plt.savefig( outPath + "/errMean" )

    plt.clf()
    ax = fig.gca(projection='3d')
    ax.plot_surface( X, Y, medianErr[:,:].squeeze().transpose()*100, cmap=cm.coolwarm )
    ax.set_xlabel("dist from visible (cm)")
    ax.set_ylabel("deformation (cm)")
    ax.set_zlabel("median err (cm)")
    plt.savefig( outPath + "/errMedian" )



    plt.clf()
    ax = fig.gca(projection='3d')
    ax.plot_surface( X, Y, minErrRel[:,:].squeeze().transpose(), cmap=cm.coolwarm )
    ax.set_xlabel("dist from visible")
    ax.set_ylabel("deformation (cm)")
    ax.set_zlabel("min errRel (cm)")
    plt.savefig( outPath + "/errMinRel" )

    plt.clf()
    ax = fig.gca(projection='3d')
    ax.plot_surface( X, Y, maxErrRel[:,:].squeeze().transpose(), cmap=cm.coolwarm )
    ax.set_xlabel("dist from visible")
    ax.set_ylabel("deformation (cm)")
    ax.set_zlabel("max errRel (cm)")
    plt.savefig( outPath + "/errMaxRel" )

    plt.clf()
    ax = fig.gca(projection='3d')
    ax.plot_surface( X, Y, meanErrRel[:,:].squeeze().transpose(), cmap=cm.coolwarm )
    ax.set_xlabel("dist from visible")
    ax.set_ylabel("deformation (cm)")
    ax.set_zlabel("mean errRel (cm)")
    plt.savefig( outPath + "/errMeanRel" )

    plt.clf()
    ax = fig.gca(projection='3d')
    ax.plot_surface( X, Y, medianErrRel[:,:].squeeze().transpose(), cmap=cm.coolwarm )
    ax.set_xlabel("dist from visible")
    ax.set_ylabel("deformation (cm)")
    ax.set_zlabel("median errRel (cm)")
    plt.savefig( outPath + "/errMedianRel" )

    plt.clf()
    #for i in range( 0, int(binsDist/2), 5 ):
    for i in [0,2,4,6]:
        dist = i/binsDist*maxDist
        col = (1-float(i)/float(binsDist/2)*0.9, 0.2, 0.2 )
        #plt.plot( y, minErr[i,:], '.', label = "min {:.2f}m".format(dist), color = col )
        #plt.plot( y, maxErr[i,:], '.', label = "train {:.2f}m".format(dist), color = col )
        myY = []
        mean = []
        median = []
        maximum = []
        minimum = []
        for j in range( meanErr.shape[1]-1 ):
            if meanErr[i,j] != 0 and medianErr[i,j] != 0:
                myY.append( y[j] )
                mean.append( meanErr[i,j]*100 )
                median.append( medianErr[i,j]*100 )
                maximum.append( maxErr[i,j]*100 )
                minimum.append( minErr[i,j]*100 )
        plt.plot( myY, mean, '-', label = "mean {:.0f}cm".format(dist*100), color = col )
        #plt.plot( myY, median, '--', label = "median {:.0f}cm".format(dist*100), color = col )
        #plt.plot( myY, maximum, '.-', label = "maximum {:.0f}cm".format(dist*100), color = col )
        #plt.plot( myY, minimum, '.-', label = "minimum {:.0f}cm".format(dist*100), color = col )
    plt.xlabel( "deformation (cm)" )
    plt.ylabel( "mag. of error (cm)" )
    plt.legend()
    plt.grid(True)
    plt.savefig( outPath + "/err" )

    plt.clf()
    #for i in range( 0, int(binsDist/2), 5 ):
    #for i in range( 0, int(binsDist/2), 5 ):
    for i in [0,2,4,6]:
        dist = i/binsDist*maxDist
        col = (1-float(i)/float(binsDist/2)*0.9, 0.2, 0.2 )
        #plt.plot( y, minErrRel[i,:], '--', label = "min {:.2f}m".format(dist), color = col )
        #plt.plot( y, maxErrRel[i,:], '--', label = "train {:.2f}m".format(dist), color = col )
        myY = []
        mean = []
        median = []
        for j in range( meanErrRel.shape[1]-1 ):
            if meanErrRel[i,j] != 0 and medianErrRel[i,j] != 0:
                myY.append( y[j] )
                mean.append( meanErrRel[i,j] )
                median.append( medianErrRel[i,j] )
        plt.plot( myY, mean, '-', label = "mean {:.0f}cm".format(dist*100), color = col )
        #plt.plot( myY, median, '--', label = "median {:.0f}cm".format(dist*100), color = col )
    plt.legend()
    plt.xlabel( "deformation (cm)" )
    plt.ylabel( "mag. of error (rel)" )
    plt.grid(True)
    plt.savefig( outPath + "/errRel" )

    metrics = numpy.load( path + "/metrics.npy" )
    plt.figure("errors")
    plt.title( "Average displacement error" )
    plt.xlabel( "deformation (cm)" )
    plt.ylabel( "mag. of error (cm)" )

    plt.figure("counts")
    plt.title( "Number of voxels" )
    plt.xlabel( "deformation (cm)" )
    plt.ylabel( "voxels" )
    plt.semilogy()
  
    numDisplacementBins = 8
    fullNumPerBin = []
    for i in range( numDisplacementBins ):
        fullNumPerBin.append( 0 )

    font = {'family' : 'normal',
        #'weight' : 'bold',
        'size'   : 14}
    matplotlib.rc('font', **font)

    fig = plt.figure("confusionGrid", figsize=(12, 5.5))
    #grid = AxesGrid(fig, 111,
    #            nrows_ncols=(2, 3),
    #            axes_pad=0.10,
    #            cbar_mode='single',
    #            cbar_location='right',
    #            cbar_pad=0.1
    #            )

    #fig = plt.figure("averageErrors",figsize=(12, 8))
    #gridAvgErrors = AxesGrid(fig, 111,
                #nrows_ncols=(1, 3),
                #axes_pad=0.10,
                #cbar_mode='none',
                #cbar_location='right',
                #cbar_pad=0.1
                #)
    gs = gridspec.GridSpec(2, 4, height_ratios=(1, 3), width_ratios=(5, 5, 5, 1), wspace=0.05, hspace=0.001 )

    # first graph
    #axes = plt.subplot(gs[0,0])
    #pc = plt.pcolor(df1, cmap='jet')

    # second graph
    #axes = plt.subplot(gs[1,0])
    #plt.pcolor(df2, cmap='Greys')

    # colorbar
    #axes = plt.subplot(gs[0,1])
    #plt.colorbar(pc, cax=axes)

    #plt.clf()
    imAxList = []
    maxAvgErr = 0
    avgAxList = []
    avgAx = None
    for dist in [2,4,6]:
        print("----------------")
        print("Distance from visible displacement: {}".format(dist))
        numBins = 50
        confusionMatrix = numpy.zeros( (numBins, numBins) )
        maxDisplacement = 0.1
        minDist = (dist - 0.5)*1e-2
        maxDist = (dist + 0.5)*1e-2
        targetDisplacements = []
        errorLists = []
        for i in range(numBins):
            errorLists.append([])
        for i in range( 0, int(len(metrics)) ):
            m = metrics[i]
            mDist = m[2]
            mErr = m[3]
            displ = m[0]
            displTarget = m[1]
            #print(displTargetBin, displTarget)
            if mDist > minDist and mDist < maxDist:
                if displTarget < maxDisplacement:
                    displTargetBin = int(displTarget/maxDisplacement*numBins)
                    errorLists[displTargetBin].append( mErr*1e2 )
                    if displ < maxDisplacement:
                        displBin = int(displ/maxDisplacement*numBins)
                        confusionMatrix[displTargetBin][displBin] += 1
        avgErrors = []
        for i in range(numBins):
            if len(errorLists[i]) > 0:
                avgErrors.append( sum(errorLists[i])/len(errorLists[i]) )
                targetDisplacements.append( i/numBins*maxDisplacement*100 )
        if max(avgErrors) > maxAvgErr:
            maxAvgErr = max(avgErrors)

        #ax = grid[3+int(dist/2)-1]
        ax = plt.subplot(gs[1,int(dist/2)-1] )
        im = ax.imshow( numpy.transpose( confusionMatrix ), origin="lower", extent=(0,maxDisplacement*1e2,0,maxDisplacement*1e2), norm=LogNorm(), vmin=1, vmax=1e5, cmap="Oranges" )
        ax.set_xticks( [0,2,4,6,8] )
        ax.set_yticks( [2,4,6,8] )
        ax.set_xlabel( "$\Vert u_{tar}(p) \Vert$ [cm]" )
        if dist == 2:
            ax.set_ylabel( "$\Vert u_{est}(p) \Vert$ [cm]" )
        else:
            ax.set_yticklabels( [] )
        ax.grid()
        imAxList.append(ax)
        #matplotlib.image.imsave( outPath + "/heatMap{}.png".format(dist), confusionMatrix )

        #ax = grid[int(dist/2)-1]
        if avgAx is None:
            avgAx = fig.add_subplot(gs[0,int(dist/2)-1] )
        else:
            avgAx = fig.add_subplot(gs[0,int(dist/2)-1], sharey = avgAx)
        #ax = gridAvgErrors[int(dist/2)-1]
        avgAx.plot( targetDisplacements, avgErrors )
        avgAx.set_xlim( ax.get_xlim() )
        #ax.plot( numpy.arange(0,10), numpy.arange(0,10) )
        avgAx.set_title( "$d(p)={}\pm 0.5 cm$".format( dist ) )
        #fig.savefig( outPath + "/avgErrors{}.pdf".format( dist ), format="pdf" )
        #ax.set_title( "Distance: ${}\pm 0.5cm$".format( dist ) )
        avgAx.set_xticks( [0,2,4,6,8] )
        avgAx.set_xticklabels( [] )
        avgAx.yaxis.set_major_locator(ticker.MultipleLocator(1))
        #avgAx.set_ylim( 0, 3 )
        #avgAx.set_yticks( (0,1,2,3) )
        if dist == 2:
            avgAx.set_ylabel( "Avg. $E(p)$ [cm]" )
        else:
            pass
            #avgAx.set_yticklabels( [] )
            #plt.setp(avgAx.get_xticklabels(), visible=False)

        avgAx.grid()
        avgAxList.append(avgAx)

        
        fConfMatrix = open( outPath + "/confusion{}.dat".format(dist), "w" )
        for x in range(confusionMatrix.shape[0]):
            for y in range(confusionMatrix.shape[1]):
                val = confusionMatrix[x][y]
                if val < 1:
                    val = "nan"
                displTarget = x/numBins*maxDisplacement
                displ = y/numBins*maxDisplacement
                fConfMatrix.write( "({},{},{})\n".format( displTarget, displ, val ) )
            #fConfMatrix.write( "\n" )
        fConfMatrix.close()

    for i in range( len( avgAxList ) ):
        if i == 0:
            plt.setp( avgAxList[i].get_yticklabels(), visible=True)
        else:
            plt.setp( avgAxList[i].get_yticklabels(), visible=False)
    #for i in range( 
    #fig.subplots_adjust(right=0.9)
    # Colorbar same height as axis:
    #cbar_ax = fig.add_axes([0.95, extent.y0, 0.03, extent.y1-extent.y0])
    #divider = make_axes_locatable(ax)
    #cbar_ax = divider.append_axes("right", size="5%", pad=0.05)
    #plt.colorbar(im, cax=cax)
    #cbar = fig.colorbar(im, cax=cbar_ax)

    #cbar = ax.cax.colorbar(im)
    #cbar = ax.

    #ax.images[0].colorbar.set_clim( 1, 1e6 )

    plt.figure("confusionGrid")
    p = ax.get_position()
    #cbar_ax = fig.add_subplot(gs[1,3])
    p.x0 = p.x1 + 0.01
    p.x1 = p.x0 + 0.02
    p.y0 = p.y0 + 0.033
    p.y1 = p.y1 - 0.033
    cbar_ax = plt.axes( p )
    cbar = plt.colorbar(im, cax=cbar_ax, ticks = LogLocator() )
    #cbar = plt.colorbar(im, cax=cbar_ax, ticks = LogLocator())
    #cbar_ax = fig.add_subplot(gs[0,3])
    #cbar = plt.colorbar(im, cax=cbar_ax, ticks = LogLocator())
    #cbar = grid.cbar_axes[0].colorbar(im)
    #cax = cbar.ax
    #axis = cax.axis[cax.orientation]
    #grid[2].cax.set_yticks([1e0,1e1,1e2,1e3,1e4,1e5])
    #grid[2].cax.set_yticks([1e1,1e2,1e3,1e4,1e5])
    #grid[2].cax.set_yticklabels(["$10^1$","$10^2$","$10^3$","$10^4$","$10^5$"], fontsize=17 )
    #grid[2].cax.yaxis.set_major_formatter(StrMethodFormatter('{x:.1e}'))
    #cbar.ax.set_ticks(LogLocator())
    #minorticks = p.norm(np.arange(1, 10, 2))
    #cb.ax.xaxis.set_ticks(minorticks, minor=True)
    #cbar.ax.xaxis.set_ticks( LogLocator() )
    #cbar_ax.set_yticks([1e1,1e2,1e3,1e4,1e5])
    #cbar_ax.set_yticklabels(["$10^1$","$10^2$","$10^3$","$10^4$","$10^5$"], fontsize=17 )
    current_cmap = matplotlib.cm.get_cmap()
    current_cmap.set_bad(color='white')
    plt.savefig( outPath + "/confusion.pdf", bbox_inches='tight', format="pdf" )

    #plt.figure("averageErrors")
    #plt.savefig( outPath + "/avgErrors.pdf", bbox_inches='tight', format="pdf" )
    #plt.savefig( outPath + "/avgErrors.pdf", format="pdf" )
#    for dist in [2,4,6]:
#        print("----------------")
#        print("Distance from visible displacement: {}".format(dist))
#        displacements = []
#        displacementBins = []
#        minDispl = 0.01
#        maxDispl = 0.08
#        minDist = (dist - 0.5)*1e-2
#        maxDist = (dist + 0.5)*1e-2
#        avgError = 0
#        avgDisplacement = 0
#        f = open( outPath + "/barPlots{}cm.tex".format(dist), 'w')
#        fAvg = open( outPath + "/avgDisplacementErrors{}cm.tex".format(dist), 'w')
#        fAvg.write("#displacement\tavgDisplacementError\n")
#        for i in range(0,numDisplacementBins):
#            displacementBins.append( [] )
#        for i in range( 0, len(metrics) ):
#            m = metrics[i]
#            mErr = m[0]
#            mDispl = m[1]
#            mDist = m[2]
#            if mDist >= minDist and mDist <= maxDist:
#                binID = int((mDispl-minDispl)/maxDispl*numDisplacementBins + 0.5)
#                if binID >= 0 and binID < len(displacementBins):
#                    displacementBins[binID].append( mErr )
#            
#                avgError += mErr
#                avgDisplacement += mDispl
#        num = []
#        avg = []
#        for i in range( numDisplacementBins ):
#            if len(displacementBins[i]) > 0:
#                displacements.append( (i/numDisplacementBins*maxDispl + minDispl)*1e2 )
#                num.append( len(displacementBins[i]) )
#                avg.append( sum(displacementBins[i])/len(displacementBins[i])*1e2 )
#                fAvg.write( "{}\t{}\n".format( displacements[-1], avg[-1] ) )
#            fullNumPerBin[i] += len(displacementBins[i])
#
#        plt.figure("errors")
#        plt.plot( displacements, avg, '-o', label = "avg {} cm".format(dist), color = (1 - dist/10,0.2,0.2) )
#        plt.figure("counts")
#        plt.plot( displacements, num, '-o', label = "{} cm".format(dist), color = (1 - dist/10,0.2,0.2) )
#        print( "Average error at {} cm: {} (average displacement: {})".format( dist, avgError/len(metrics), avgDisplacement/len(metrics) ) )
#
#        
#        for i in range(0,numDisplacementBins):
#            dispBin = numpy.array(displacementBins[i])
#            if len(dispBin) > 0:
#                median = numpy.median( dispBin )
#                quantile1 = numpy.percentile( dispBin, 25 )
#                quantile3 = numpy.percentile( dispBin, 75 )
#                minimum = dispBin.min()
#                maximum = dispBin.max()
#                position = 1 + i - 0.4 + (dist/2)*0.2
#                print( minimum, quantile1, median, quantile3, maximum )
#                f.write("\\addplot +[boxplot prepared={{\n"
#                        "draw position={},\n"
#                        "box extend=0.2,\n"
#                        "lower whisker={},\n"
#                        "lower quartile={},\n"
#                        "median={},\n"
#                        "upper quartile={},\n"
#                        "upper whisker={}\n"
#                        "}},d{},solid] coordinates {{}};\n".format( position, minimum*1e2, quantile1*1e2, median*1e2, quantile3*1e2, maximum*1e2, dist ) )
#                #plt.figure("bp")
#                #plt.boxplot(displacementBins)
#                #plt.savefig( outPath + "/bp{}_{}".format(dist,i) )
#
    plt.figure("errors")
    plt.grid(True)
    plt.legend()
    plt.savefig( outPath + "/errAll" )

    plt.figure("counts")
    plt.grid(True)
    plt.legend()
    plt.savefig( outPath + "/numAll" )

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise ValueError("Usage: python3 generateErrorPlots.py PathToResultFolder")

    outPath = sys.argv[1]
    print( "Loading from: " + outPath )
    parseDirectory( outPath )


