import os,sys 
if len(sys.argv) < 3:
    raise ValueError("Usage: python3 apply.py PathToData PathToModel [OutputPath [StartIndex Num]]")

import numpy
import math
import data
from data import SimulatedDataSet
import checkpoint
from const import *
from Network import Network
import torch
import torch.nn as nn
from torch.autograd import Variable
import time
import magnitudeLoss
import generateErrorPlots
import generateErrorPlotsSortedByVisDispl
import util

dataPath = sys.argv[1]
modelPath = sys.argv[2]
outPath = os.path.dirname("./output")
if len(sys.argv) > 3:
    outPath = sys.argv[3]
if not os.path.exists(outPath):
    os.makedirs(outPath)

startIndex = 0
num = 100
if len(sys.argv) > 4:
    startIndex = int(sys.argv[4])
if len(sys.argv) > 5:
    num = int(sys.argv[5])

print( "Saving results in: ", outPath )

#autoFindZeroDisplacement = False

net = Network()
#testData = SimulatedDataSet( dataPath, num, startIndex, maxDefThreshold=0.1, augment=False )
testData = SimulatedDataSet( dataPath, num, startIndex, maxDefThreshold=0.1, augment=False) #visPercentage=100 )
print("Loaded {:d} test samples".format(len(testData)))
#testDataLoader = torch.utils.data.DataLoader( testData, batch_size=10, shuffle=False, num_workers=0 )
#testData = data.loadSimData( dataPath, num, startIndex )
#testData = data.removeLargeDeformations( testData, 0.1)
#testData = data.toTorch( testData, cuda=True )

#criterion0 = nn.MSELoss(size_average=False)
#criterion1 = nn.MSELoss(size_average=False)
#criterion2 = nn.MSELoss(size_average=False)
#criterion3 = nn.MSELoss(size_average=False)
#criterion0.cuda()
#criterion1.cuda()
#criterion2.cuda()
#criterion3.cuda()

avgPool64to32 = torch.nn.AvgPool3d(2, stride=2)
avgPool64to16 = torch.nn.AvgPool3d(4, stride=4)
avgPool64to8 = torch.nn.AvgPool3d(8, stride=8)

net = net.cuda()
net = torch.nn.DataParallel(net)

if modelPath is not None:
    print("Loading from checkpoint in: " + modelPath)
    startEpoch, trainErrs, testErrs = checkpoint.load( modelPath, net )
else:
    raise ValueError( "No path to model given." )

def isBorderOfZeroDispl( index, surfaceWithoutZeroDispl ):
    #print("Check index:", index)
    for dx in range(-1,2):
        for dy in range(-1,2):
            for dz in range(-1,2):
                x = index[0] + dx
                y = index[1] + dy
                z = index[2] + dz
                if dx == dy and dy == dz:
                    continue

                if x >= 0 and x < 64 and y >= 0 and y < 64 and z >= 0 and z < 64:
                    #print("\t",x.item(),y.item(),z.item(),surfaceWithoutZeroDispl[x,y,z].item())
                    if surfaceWithoutZeroDispl[x,y,z] > 0.5:
                        #print("\tTrue")
                        return True
    #print("\tFalse")
    return False

        

lossSum = numpy.array([0.0,0.0,0.0,0.0,0.0,0.0])
maxLoss = -1
maxLossIndex = -1
maxDispErr = -1
maxDispErrIndex = -1
maxDispForMaxDispErr = -1
resultData = []
with torch.no_grad():
    for i in range( 0, len(testData) ):
    #for i_batch, d in enumerate(testDataLoader):

        # If we're looking for a good zero displacemnt, do multiple iterations. Otherwise, don't:
        #zeroDisplacementIterations = 1
        #if autoFindZeroDisplacement:
        #    zeroDisplacementIterations = 10
        #    lowestVisDisplLoss = 9999
        #    lowestVisDisplLossResult = None
        #zeroDisplacement = None

        d = testData[i]
        print(d["path"])

        #for j in range( zeroDisplacementIterations ):
            #if autoFindZeroDisplacement:
            #    print("Testing zero displacement iteration:", j)

        # Forward pass and loss calculation:
        mesh = d["mesh"].cuda().unsqueeze(0)
        #visDispl = Variable( d["visDispl" + visiblePercentage].cuda() )
        visDispl = d["visDispl"].cuda().unsqueeze(0)
        target64 = d["target"].cuda().unsqueeze(0)
        target64.requires_grad_( False )
        target32 = avgPool64to32( target64 )
        target16 = avgPool64to16( target64 )
        target8 = avgPool64to8( target64 )

        # If we're auto-finding zero displacement, update here:
        #if autoFindZeroDisplacement:
        #    if j == 0:
        #        zeroDisplacement = mesh[0,1,:]
        #        surfaceWithoutZeroDispl = d["surface"].squeeze() * (1-zeroDisplacement)
        #    else:
        #        #zeroDisplacementPrevious = zeroDisplacement.clone()
        #        nonZero = torch.nonzero( zeroDisplacement )
        #        #print( "zeroDisplacement voxels:", nonZero.shape[0] )
        #        if nonZero.shape[0] == 0:
        #            break
        #        #print("surf1:", torch.nonzero( d["surface"] ).shape[0] )
        #        surfaceWithoutZeroDispl = d["surface"].squeeze() * (1-zeroDisplacement)
        #        #print("surf2:", torch.nonzero( surfaceWithoutZeroDispl ).shape[0] )
        #        for ind in nonZero:
        #            #print(ind)
        #            if isBorderOfZeroDispl( ind, surfaceWithoutZeroDispl ):
        #                #print(zeroDisplacement[ind[0],ind[1],ind[2]])
        #                zeroDisplacement[ind[0],ind[1],ind[2]] = 0
        #        mesh[0,1,:] = zeroDisplacement
        #        #print( "zeroDisplacement2:", torch.nonzero(zeroDisplacement).shape[0] )

        t1 = time.time()
        out64, out32, out16, out8 = net.forward( mesh, visDispl )
        torch.cuda.synchronize()
        dt = time.time() - t1

        visMask = (visDispl.abs().sum( dim=1 ) > 0).float()

        loss, l64, l32, l16, l8, lVis = magnitudeLoss.loss( out64, out32, out16, out8,
                target64, target32, target16, target8, visMask )

        # Individual errors:
        lossSum[0] += l64.item()
        lossSum[1] += l32.item()
        lossSum[2] += l16.item()
        lossSum[3] += l8.item()
        lossSum[4] += lVis.item()
        # Total err:
        lossSum[5] += loss.item()


        displacementError = torch.norm( out64-target64, 2, 1 )
        targetMag = torch.norm( target64, 2, 1 )
        #displacementMag = torch.norm( out0, 2, 1 )
        displacementErrorRel = torch.zeros_like( displacementError )
        displacementErrorRel[targetMag > 1e-6] = displacementError[targetMag > 1e-6]/targetMag[targetMag > 1e-6]
        #print(displacementError)

        d["output"] = out64.data.cpu().numpy().squeeze()
        d["displacementError"] = displacementError.data.cpu().numpy().squeeze()
        d["displacementErrorRel"] = displacementErrorRel.data.cpu().numpy().squeeze()

        #if autoFindZeroDisplacement:
        #    #if j == zeroDisplacementIterations-1:
        #    d["suffix"] = j
        #    d["zeroDisplacement"] = zeroDisplacement.data.cpu().numpy().squeeze()
        #    d["tmpSurface"] = surfaceWithoutZeroDispl.data.cpu().numpy().squeeze()
        #    dCopy = {}
        #    for k,v in d.items():
        #        if isinstance(v,torch.Tensor):
        #            dCopy[k] = v.clone()
        #        elif isinstance(v,numpy.ndarray):
        #            dCopy[k] = numpy.copy( v )
        #        else:
        #            #print("\t",v)
        #            dCopy[k] = v
        #    resultData.append( dCopy )

        #    if lowestVisDisplLoss > lVis.item():
        #        lowestVisDisplLoss = lVis.item()
        #        lowestVisDisplLossResult = dCopy
        #        print("New lowest visible loss", lowestVisDisplLoss )
        #else:
        #    resultData.append(d)
            #d["zeroDisplacement"] = zeroDisplacement.data.cpu().numpy().squeeze()
        resultData.append(d)

        #diff = ( d["displacementError"] - d["displacementErrorRel"] )
        

        if l64.item() > maxLoss:
            maxLoss = l64.item()
            maxLossIndex = d["index"]

        dispErr = displacementError.max().item()
        if dispErr > maxDispErr:
            maxDispErr = dispErr
            maxDispErrIndex = d["index"]
            maxDispForMaxDispErr = torch.norm( target64, 2, 1 ).max().item()


        print(i, "Loss: {:e}, {:e}, {:e}, {:e}, Vis: {:e}\tTotal: {:e}".format( l64.item(), l32.item(), l16.item(), l8.item(), lVis.item(), loss.item() ) )
        print("\tTime: {:.2e}s".format( dt ) )
        #if autoFindZeroDisplacement:
        #    print("\tVisible Loss:", lVis.item() )
        del mesh, visDispl, target64, target32, target16, target8, loss, l64, l32, l16, l8, lVis, out64, out32, out16, out8, visMask
        del displacementError, targetMag, displacementErrorRel
        #cudaTensorsToDelete = []
        #for k,v in d.items():
        #    print(k, type(v))
        del d
        
        #if autoFindZeroDisplacement:
        #    if lowestVisDisplLossResult is not None:
        #        del lowestVisDisplLossResult["suffix"]
        #        resultData.append( lowestVisDisplLossResult )


avgTestErrs = lossSum/len(testData)

print("Average Loss: {:e}, {:e}, {:e}, {:e} Vis: {:e} \tTotal: {:e}".format( avgTestErrs[0], avgTestErrs[1], avgTestErrs[2], avgTestErrs[3], avgTestErrs[4], avgTestErrs[5] ) )
print("Max Loss 0: {:e}, (Sample {:d})".format( maxLoss, maxLossIndex ) )
print("Max Displacement Error: {:e} (Displacement: {:e}, Sample {:d})".format( maxDispErr, maxDispForMaxDispErr, maxDispErrIndex ) )

data.saveResults( resultData, outPath, startIndex )
generateErrorPlots.parseDirectory( outPath )
generateErrorPlotsSortedByVisDispl.parseDirectorySortByVisDispl( outPath )

