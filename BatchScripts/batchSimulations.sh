#!/bin/bash

command -v voxelize >/dev/null 2>&1 || { echo >&2 "The 'voxelize' tool must be built and in your PATH. Check the Readme for building instructions. Aborting."; exit 1; }
command -v vtk2elmer >/dev/null 2>&1 || { echo >&2 "The 'vtk2elmer' tool must be built and in your PATH. Check the Readme for building instructions. Aborting."; exit 1; }
command -v simulationGen >/dev/null 2>&1 || { echo >&2 "The 'simulationGen' tool must be built and in your PATH. Check the Readme for building instructions. Aborting."; exit 1; }
command -v ElmerSolver >/dev/null 2>&1 || { echo >&2 "The 'ElmerSolver' tool must be installed and in your PATH. Aborting."; exit 1; }

if [ "$#" -lt 3 ]; then
	echo "Usage: ./batchSimulations.sh inputFile|None outputFolder numberOfSimulations [cubeSideLength [startIndex]]"
	exit 1;
fi
inputFile=$1
outputFolder=$2
num=$3

BASEDIR=$(pwd)

echo "Input file: $inputFile"
echo "Will generate and run $num simulations."
echo "Placing simulations in $outputFolder."

generateRandom=false
if [ "$inputFile" == "None" ]; then
	extension="vtk"
	generateRandom=true
  # Check if blender can be found:
  command -v blender >/dev/null 2>&1 || { echo >&2 "To generate random meshes, 'blender' is required. Either install it and make sure it is in your PATH, or provide an input mesh. Aborting."; exit 1; }
  command -v gmsh >/dev/null 2>&1 || { echo >&2 "To generate random meshes, 'gmsh' is required. Either install it and make sure it is in your PATH, or provide an input mesh. Aborting."; exit 1; }
	echo "No input file. Will generate random meshes."
else
	filename=$(basename "$inputFile")
	extension="${filename##*.}"
	filename="${filename%.*}"
	echo "Received input mesh: $inputFile"
fi

cubeSideLength=10;
if [ "$#" -gt 3 ]; then
	cubeSideLength=$4;
fi
startIndex=0;
if [ "$#" -gt 4 ]; then
	startIndex=$5;
fi

echo "Extension $extension"

for i in `seq $startIndex $(($num+$startIndex-1))`;
do
	echo "-------------------------------------"
	echo "Simulation: $i"
	# Set up folder
	subFolder=$(printf "%05i" $i)
	curFolder=$outputFolder/$subFolder/
	mkdir $curFolder -p

	# Generate a random input mesh:
	if [ $generateRandom == true ]; then
		echo "Generating random mesh:"
		blender --background -noaudio --python generateRandomMesh.py -- $(($i+1)) $curFolder
		cd $curFolder
		echo "Merge \"randomMesh.stl\";" > randomMesh.geo
		echo "Mesh.Algorithm3D = 1;" >> randomMesh.geo 	# Delaunay
		echo "Mesh.Optimize = 1;" >> randomMesh.geo
		echo "Mesh.OptimizeNetgen = 1;" >> randomMesh.geo
		echo "Mesh.CharacteristicLengthMax = 0.01;" >> randomMesh.geo
		echo "Surface Loop(1) = {1};" >> randomMesh.geo
		echo "Volume(1) = {1};" >> randomMesh.geo
		gmsh randomMesh.geo -o input.vtk -3
		cd $BASEDIR
	else
		# Copy simulation input file:
		cp $inputFile $curFolder/input.$extension
	fi


	# Set up random boundary conditions for the folder:
	echo "Running simulationGen:"
	simulationGen $curFolder $(($i+1))

	# Generate simulation files:
	echo "Running vtk2elmer:"
	vtk2elmer $curFolder

	# Run the simulation:
	echo "Running ElmerSolver:"
	mkdir $curFolder/simulation/result/ -p
	cd $curFolder/simulation/
	ElmerSolver case.sif
	#ElmerSolver case.sif > solver.log

	cd $BASEDIR

	# Voxelize the input:
	#echo "Running voxelize on input:"
	#voxelize $curFolder/input.$extension $cubeSideLength 0.025 $curFolder -b $curFolder/surface.vtu -s 0.3
	#voxelize $curFolder/input.$extension $cubeSideLength 0.025 $curFolder -b $curFolder/surface.vtu -s 0.3
	# Voxelize the output:
	echo "Running voxelize on output:"
	voxelize $curFolder/simulation/result/case0001.vtu $cubeSideLength 0.025 $curFolder -s 0.3 -b $curFolder/simulation.vtu -m 0.1

done
