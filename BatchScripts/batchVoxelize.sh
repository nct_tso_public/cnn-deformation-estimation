#!/bin/bash

command -v voxelize >/dev/null 2>&1 || { echo >&2 "The 'voxelize' tool must be built and in your PATH. Check the Readme for building instructions. Aborting."; exit 1; }

if [ "$#" -lt 3 ]; then
	echo "Usage: ./batchVoxelize.sh inputFolder outputFolder numberOfSimulations [cubeSideLength [startIndex]]"
	exit 1;
fi
inputFolder=$1
outputFolder=$2
num=$3

BASEDIR=$(pwd)

echo "Re-Voxelizing data in $inputFolder, placing new voxel grids in $outputFolder."
echo "Will generate voxel grids of side length $cubeSideLength."

cubeSideLength=10;
if [ "$#" -gt 3 ]; then
	cubeSideLength=$4;
fi
startIndex=0;
if [ "$#" -gt 4 ]; then
	startIndex=$5;
fi

extension="vtk"

for i in `seq $startIndex $(($num+$startIndex-1))`;
do
	echo "-------------------------------------"
	echo "Simulation: $i"
	# Set up folder
	subFolder=$(printf "%05i" $i)
	curInputFolder=$inputFolder/$subFolder/
	curOutputFolder=$outputFolder/$subFolder/
	mkdir $curOutputFolder -p

	# Voxelize the input:
	#echo "Running voxelize on input:"
	#voxelize $curInputFolder/input.$extension $cubeSideLength 0.025 $curOutputFolder -b $curInputFolder/surface.vtu -s 0.3
	# Voxelize the output:
	echo "Running voxelize on output:"
	voxelize $curInputFolder/simulation/result/case0001.vtu $cubeSideLength 0.025 $curOutputFolder -s 0.3 -b $curInputFolder/simulation.vtu -m 0.1

done
